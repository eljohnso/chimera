import sys
import traceback
# from madxtools import transfer_function

# Redirect error messages to a file
sys.stderr = open("error_log.txt", "w")

try:

    from tkinter import Tk, Label, Button, Entry, ttk, Frame
    import numpy as np

    # Configure the default font size
    main_fontsize = 13

    def calculate_values(event=None):
        E_cin_per_nucleon = float(entry.get())

        # Ion properties
        A = 208.0
        Z = 82.0
        N = 126.0
        charge = 54.0
        m_proton_GeV = 0.93828
        m_neutron_GeV = 0.93957
        m_electron_GeV = 0.000511
        m_u_GeV = 0.9315
        mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
        E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV

        p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)

        rho = 70.0789
        Brho = p / charge

        E_cin_proton = np.sqrt(Brho ** 2 + m_proton_GeV ** 2) - m_proton_GeV

        B = 3.3356*p/(rho*charge)*10000

        output_label.config(text=f"Kinetic energy per nucleon: {round(E_cin_per_nucleon, 3)} GeV/u\n"
                                f"E0 or rest mass of ion: {round(E_0, 3)} GeV/c^2\n"
                                f"Ion beam momentum: {round(p, 3)} GeV/c\n"
                                f"Rigidity (Brho): {round(Brho, 3)} GeV/c (also called equivalent proton momentum)\n"
                                f"Rigidity (Brho): {round(Brho * 3.3356, 3)} T*m\n"
                                f"\nA proton beam at this rigidity would have a kinetic energy of {round(E_cin_proton, 3)} GeV\n"
                                f"B-field = {round(B,3)} G")

        # optics = [0.47969,
        #   0.1149,
        #   0.134,
        #   0.09035,
        #   0.1290730560000001,
        #   0.12815110559999993,
        #   0.04043608600800003,
        #   0.01]

        # output_current_label.config(text="current QFN1 = "+str(round(transfer_function.current(abs(optics[0]), "Q74L", Brho),3))+";\n"
                                # f"E0 or rest mass of ion: {round(E_0, 3)} GeV/c^2\n"
        # )
#     print("current QFN1 = "+str(round(transfer_function.current(abs(optics[0]), "Q74L", Brho),3))+";")
# print("current QDN2 = "+str(round(transfer_function.current(abs(optics[1]*54/82), "Q120C", Brho),3))+";")
# print("current QFN3 = "+str(round(transfer_function.current(abs(optics[2]*54/82), "QFL", Brho),3))+";")
# print("current QDN4 = "+str(round(transfer_function.current(abs(optics[3]*54/82), "QFS", Brho),3))+";")
# print("current QFN5 = "+str(round(transfer_function.current(abs(optics[4]*54/82), "QFL", Brho),3))+";")
# print("current QDN6 = "+str(round(transfer_function.current(abs(optics[5]*54/82), "QFL", Brho),3))+";")
# print("current QDN7 = "+str(round(transfer_function.current(abs(optics[6]*54/82), "Q200L", Brho),3))+";")
# print("current QFN8 = "+str(round(transfer_function.current(abs(optics[7]*54/82), "Q200L", Brho),3))+";")

        
    def bfield_to_Ekin(event=None):
        B = float(entry2.get())

        Ekin = (193.737692/208)*(np.sqrt(((1/((3.3356/(70.0789*54)*10000)/B))/193.737692)**2+1)-1)

        output_label2.config(text=f"Ekin: {round(Ekin, 3)} GeV/u\n")

    root = Tk()
    root.title("Ion Beam Properties Calculator")
    root.configure() # Set the background color
    root.geometry("600x300")
    #root.iconbitmap("atom.ico") # Set the icon

    # Create a style
    style = ttk.Style()

    # Configure the Tab style, including the background color of the active tab
    style.configure("TNotebook.Tab",
                    padding=[100, 5])  # Add some padding

    # Configure the Notebook style, including the background color of the tab area
    style.configure("TNotebook",
                    tabmargins=[2, 5, 2, 0],  # Add some margins
                    )  # Change the background color of the tab area

    notebook = ttk.Notebook(root, style="TNotebook")
    notebook.pack(fill='both', expand=True)  # Make the notebook fill the entire window


    # Create a frame for the first tab
    frame1 = Frame(notebook, width=400, height=400)
    frame1.pack(fill='both', expand=True)  # Make the frame fill the tab

    # Create the first tab with frame1 as its content
    notebook.add(frame1, text='Kin to B-field')

    # Create a frame for the second tab
    frame2 = Frame(notebook, width=400, height=400)
    frame2.pack(fill='both', expand=True)  # Make the frame fill the tab

    # Create the second tab with frame2 as its content
    notebook.add(frame2, text='B-field to Kin')


    # Entry widget for user input
    entry_label = Label(frame1, text="Enter the kinetic energy per nucleon (GeV/u):", font=("Arial", main_fontsize))
    entry_label.pack()
    entry = Entry(frame1, font=("Arial", main_fontsize))
    entry.pack()
    entry.bind("<Return>", calculate_values)  # Bind the Return key to calculate_values

    entry_label2 = Label(frame2, text="Enter the B-field (G):", font=("Arial", main_fontsize))
    entry_label2.pack()
    entry2 = Entry(frame2, font=("Arial", main_fontsize))
    entry2.pack()
    entry2.bind("<Return>", bfield_to_Ekin)  # Bind the Return key to calculate_values

    # Button to calculate values
    calculate_button = Button(frame1, text="Calculate", command=calculate_values, font=("Arial", main_fontsize))
    calculate_button.pack()

    calculate_button2 = Button(frame2, text="Calculate", command=bfield_to_Ekin, font=("Arial", main_fontsize))
    calculate_button2.pack()

    # Label to display output values
    output_label = Label(frame1, text=""  , font=("Arial", main_fontsize), justify='left')
    output_label.pack()

    # output_current_label = Label(frame1, text=""  , font=("Arial", main_fontsize), bg=bg_color, fg=fg_color, justify='left')
    # output_current_label.pack()

    output_label2 = Label(frame2, text=""  , font=("Arial", main_fontsize), justify='left')
    output_label2.pack()

    root.mainloop()

except Exception as e:
    # Print the traceback to the error log
    traceback.print_exc(file=sys.stderr)
