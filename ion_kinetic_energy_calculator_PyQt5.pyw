import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QPushButton, QVBoxLayout, QWidget
import numpy as np
from madxtools import transfer_function

def calculate_values():
    E_cin_per_nucleon = float(entry.text())

    # Ion properties
    A = 208.0
    Z = 82.0
    N = 126.0
    charge = 54.0
    m_proton_GeV = 0.93828
    m_neutron_GeV = 0.93957
    m_electron_GeV = 0.000511
    m_u_GeV = 0.9315
    mass_defect_GeV = Z * m_proton_GeV + N * m_neutron_GeV + (Z - charge) * m_electron_GeV - A * m_u_GeV
    E_0 = Z * m_proton_GeV + N * m_neutron_GeV - mass_defect_GeV

    p = E_0 * np.sqrt((((E_cin_per_nucleon * A) / E_0) + 1) ** 2 - 1)

    rho = 70.0789
    Brho = p / charge

    E_cin_proton = np.sqrt(Brho ** 2 + m_proton_GeV ** 2) - m_proton_GeV

    B = 3.3356*p/(rho*charge)*10000

    output_label.config(text=f"Kinetic energy per nucleon: {round(E_cin_per_nucleon, 3)} GeV/u\n"
                            f"E0 or rest mass of ion: {round(E_0, 3)} GeV/c^2\n"
                            f"Ion beam momentum: {round(p, 3)} GeV/c\n"
                            f"Rigidity (Brho): {round(Brho, 3)} GeV (also called equivalent proton momentum)\n"
                            f"Rigidity (Brho): {round(Brho * 3.3356, 3)} T*m\n"
                            f"\nA proton beam at this rigidity would have a kinetic energy of {round(E_cin_proton, 3)} GeV\n"
                            f"B-field = {round(B,3)} G")

    optics = [0.47969,
        0.1149,
        0.134,
        0.09035,
        0.1290730560000001,
        0.12815110559999993,
        0.04043608600800003,
        0.01]

    output_current_label.config(text="current QFN1 = "+str(round(transfer_function.current(abs(optics[0]), "Q74L", Brho),3))+";\n"
                            f"E0 or rest mass of ion: {round(E_0, 3)} GeV/c^2\n"
    )
#     print("current QFN1 = "+str(round(transfer_function.current(abs(optics[0]), "Q74L", Brho),3))+";")
# print("current QDN2 = "+str(round(transfer_function.current(abs(optics[1]*54/82), "Q120C", Brho),3))+";")
# print("current QFN3 = "+str(round(transfer_function.current(abs(optics[2]*54/82), "QFL", Brho),3))+";")
# print("current QDN4 = "+str(round(transfer_function.current(abs(optics[3]*54/82), "QFS", Brho),3))+";")
# print("current QFN5 = "+str(round(transfer_function.current(abs(optics[4]*54/82), "QFL", Brho),3))+";")
# print("current QDN6 = "+str(round(transfer_function.current(abs(optics[5]*54/82), "QFL", Brho),3))+";")
# print("current QDN7 = "+str(round(transfer_function.current(abs(optics[6]*54/82), "Q200L", Brho),3))+";")
# print("current QFN8 = "+str(round(transfer_function.current(abs(optics[7]*54/82), "Q200L", Brho),3))+";")

def bfield_to_Ekin():
    B = float(entry2.get())

    Ekin = (193.737692/208)*(np.sqrt(((1/((3.3356/(70.0789*54)*10000)/B))/193.737692)**2+1)-1)

    output_label2.config(text=f"Ekin: {round(Ekin, 3)} GeV/u\n")

app = QApplication(sys.argv)
app.setStyle('Fusion')  # Apply the Fusion style for a modern look

# Create the main window
window = QMainWindow()
window.setWindowTitle("Ion Beam Properties Calculator")

central_widget = QWidget()
window.setCentralWidget(central_widget)
layout = QVBoxLayout()
central_widget.setLayout(layout)

# Create input elements
entry_label = QLabel("Enter the kinetic energy per nucleon (GeV/u):")
entry = QLineEdit()
entry.returnPressed.connect(calculate_values)

entry_label2 = QLabel("Enter the B-field (G):")
entry2 = QLineEdit()
entry2.returnPressed.connect(bfield_to_Ekin)

# Create buttons
calculate_button = QPushButton("Calculate")
calculate_button.clicked.connect(calculate_values)

calculate_button2 = QPushButton("Calculate")
calculate_button2.clicked.connect(bfield_to_Ekin)

# Create output labels
output_label = QLabel()
output_current_label = QLabel()
output_label2 = QLabel()

# Add elements to the layout
layout.addWidget(entry_label)
layout.addWidget(entry)
layout.addWidget(calculate_button)
layout.addWidget(output_label)
layout.addWidget(output_current_label)

layout.addWidget(entry_label2)
layout.addWidget(entry2)
layout.addWidget(calculate_button2)
layout.addWidget(output_label2)

window.show()
sys.exit(app.exec_())
